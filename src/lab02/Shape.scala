package lab02

object Shape extends App {

  sealed trait Shape
  object Shape {
    case class Rectangle(width: Double, height: Double) extends Shape
    case class Circle(radius: Double) extends Shape
    case class Square(size: Double) extends Shape

    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(width, height) => 2 * width + 2 * height
      case Circle(radius) => 2 * Math.PI * radius
      case Square(size) => 4 * size
    }

    def area(shape: Shape): Double = shape match {
      case Rectangle(width, height) => width * height
      case Circle(radius) => Math.PI * radius * radius
      case Square(size) => size * size
    }
  }

  import Shape._
  val rectangle = Rectangle(10, 20)
  val circle = Circle(5)
  val square = Square(10)

  println("Perimeters:")
  println(perimeter(rectangle))
  println(perimeter(circle))
  println(perimeter(square))

  println("\nAreas:")
  println(area(rectangle))
  println(area(circle))
  println(area(square))

}
