package lab02

object Functions extends App {

  def parity(value: Int): String = value % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  val parityVal: Int => String = _ % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  def neg(predicate: String => Boolean): String => Boolean = !predicate(_)

  def genericsNeg[T](predicate: T => Boolean): T => Boolean = !predicate(_)

  val negVal: (String => Boolean) => (String => Boolean) = f => s => !f(s)

  val empty: String => Boolean = _ == ""

  val even: Int => Boolean = _ % 2 == 0

  println(parity(10))
  println(parity(1))

  println(parityVal(10))
  println(parityVal(1))

  val notEmpty = genericsNeg(empty)
  val notEven = genericsNeg(even)
  println(notEmpty("foo"))
  println(notEven(1))

}
