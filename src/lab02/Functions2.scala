package lab02

object Functions2 extends App {

  def curriedFun(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z

  def notCurriedFun(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  val currVal: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z

  val notCurrVal: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  def compose[T](f: T => T)(g: T => T): T => T = n => f(g(n))

  println(curriedFun(1)(2)(3))
  println(curriedFun(5)(2)(3))

  println(notCurriedFun(1, 2, 3))
  println(notCurriedFun(1, 5, 3))

  println(currVal(1)(2)(3))
  println(currVal(5)(2)(3))

  println(notCurrVal(1, 2, 3))
  println(notCurrVal(1, 5, 3))

  println(compose[Int](_-1)(_*2)(5)) // 9

}
