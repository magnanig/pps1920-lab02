package lab02

object Fibonacci extends App {

  def fib(n: Int): Int = n match {
    case n if n <= 1 => n
    case _ => fib(n - 2) + fib(n - 1)
  }

  println(fib(0),fib(1),fib(2),fib(3),fib(4))

}
